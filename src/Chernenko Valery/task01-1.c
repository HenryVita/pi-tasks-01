#include<stdio.h>

int main()
{
	int rub=-1,kop=-1;
	printf("Enter your sum in format *.* >> ");
	scanf ("%d.%d",&rub,&kop);
	if (rub<0 || kop<0) 
	{
		puts("Sorry, Incorrect Data Entry!!! ");
		return 1;
	}
    if (kop>=100)
	{
		rub+=kop/100;
		kop%=100;
	}
	printf("Your money: %d rub. %d kop. \n",rub,kop);
	return 0;
}