#include<stdio.h>

int main()
{
	int feet=-1,inches=-1;
	float sm;
	printf("Enter your height in format *'* >> ");
	scanf ("%d'%d",&feet,&inches);
	if (feet<0 || inches<0) 
	{
		puts("Sorry, Incorrect Data Entry!!! ");
		return 1;
	}
	inches+=feet*12;
	sm=inches*2.54;
	printf("Your height = %0.2f \n",sm);
	return 0;
}